package com.stephen.sudoku.solver.utils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IOHelper {
    public static int[][] getUnsolvedSudokuFromFile(String pathname) throws FileNotFoundException, IllegalArgumentException{
        List<String> unparsedUnsolvedSudoku = new ArrayList<String>();

        //read and keep all rows from given file
        try(Scanner scanner = new Scanner(new File(pathname))){
            while (scanner.hasNext()) {
                unparsedUnsolvedSudoku.add(scanner.nextLine());
            }
        }

        //parse the csv string into 2D-array and then return
        return parseUnsolvedSudoku(unparsedUnsolvedSudoku);
    }


    static int[][] parseUnsolvedSudoku(List<String> unsolvedSudoku) throws IllegalArgumentException{
        if(!hasValidDimension(unsolvedSudoku)){
            throw new IllegalArgumentException("The dimension of sudoku must be 9x9. Any other dimensions are not allowed.");
        }

        int[][] parsedUnsolvedSudoku = new int[9][9];
        for(int row=0; row<unsolvedSudoku.size(); row++){
            String[] unsolvedColumns = unsolvedSudoku.get(row).trim().split(",");

            for(int column=0; column<unsolvedColumns.length; column++){
                parsedUnsolvedSudoku[row][column] = Integer.parseInt(unsolvedColumns[column]);
            }
        }
        return parsedUnsolvedSudoku;
    }


    static boolean hasValidDimension(List<String> unsolvedSudoku){
        return hasValidNumberOfRow(unsolvedSudoku) && hasValidNumberOfColumn(unsolvedSudoku);
    }


    static boolean hasValidNumberOfRow(List<String> unsolvedSudoku){
        return unsolvedSudoku.size() == 9;
    }


    static boolean hasValidNumberOfColumn(List<String> unsolvedSudoku){
        for(String row : unsolvedSudoku){
            int numberOfColumns = row.trim().split(",").length;
            if(numberOfColumns != 9){
                return false;
            }
        }
        return true;
    }


    public static void printSolvedSudokuToFile(int[][] solvedSudoku, String filename) throws IOException{
        Charset ascii = Charset.forName("ASCII"); //use ascii since no special characters
        File file = new File(filename);

        try(OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(file), ascii)){
            for(String parsedSolvedSudokuRow : parseSolvedSudoku(solvedSudoku)){
                osw.write(parsedSolvedSudokuRow);
            }
        }

        System.out.println("Solved sudoku successfully written to " + file.getAbsolutePath());
    }


    static List<String> parseSolvedSudoku(int[][] solvedSudoku){
        List<String> parsedSudoku = new ArrayList<String>();

        for(int row=0; row<9; row++){
            String parsedSudokuRow = "";
            for(int column=0; column<9; column++){
                if(column != 0){
                    parsedSudokuRow += ",";
                }
                parsedSudokuRow += solvedSudoku[row][column];
            }
            if(row != 8){
                parsedSudokuRow += "\n";
            }
            parsedSudoku.add(parsedSudokuRow);
        }

        return parsedSudoku;
    }
}
