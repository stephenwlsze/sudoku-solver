package com.stephen.sudoku.solver;

import com.stephen.sudoku.solver.constructs.Position;

public class SudokuSolver {
    private int[][] grid;

    public SudokuSolver(int[][] grid){
        this.grid = grid;
    }

    public boolean hasSolution(){
        Position topLeftCorner = new Position(0, 0);
        return solve(topLeftCorner);
    }

    public int[][] getSolution(){
        return grid;
    }

    //used recursive backtracking algorithm
    boolean solve(Position currentPosition){
        int row = currentPosition.getRow();
        int column = currentPosition.getColumn();

        //finish when all position is filled
        if(row > 8){
            return true;
        }

        //skip to the next position if already contains value
        if(grid[row][column] != 0){
            return solve(getNextPosition(currentPosition));
        }

        //try to fit a valid value in current position
        for(int value=1; value<10; value++){
            if(isValidToSetValueAt(currentPosition, value)){
                grid[row][column] = value;
                if(solve(getNextPosition(currentPosition))){
                    return true;
                }
            }
        }

        //undo changes if no solution found for current position
        grid[row][column] = 0;
        return false;
    }

    Position getNextPosition(Position currentPosition){
        int currentRow = currentPosition.getRow();
        int currentColumn = currentPosition.getColumn();

        //position moves from left to right, jump to first of next row if reaches end of current row
        int nextRow = (currentColumn == 8) ? currentRow + 1 : currentRow;
        int nextColumn = (currentColumn + 1) % 9;

        return new Position(nextRow, nextColumn);
    }

    boolean isValidToSetValueAt(Position position, int value){
        int row = position.getRow();
        int column = position.getColumn();

        //sudoku rule1: no same value can be in the same row
        if(hasSameValueInRow(row, value)){
            return false;
        }

        //sudoku rule2: no same value can be in the same column
        if(hasSameValueInColumn(column, value)){
            return false;
        }

        //sudoku rule3: no same value within same region
        if(hasSameValueInRegion(position, value)){
            return false;
        }

        return true;
    }

    boolean hasSameValueInRow(int row, int value){
        for(int i=0; i<9; i++){
            if(grid[row][i] == value){
                return true;
            }
        }
        return false;
    }

    boolean hasSameValueInColumn(int column, int value){
        for(int i=0; i<9; i++){
            if(grid[i][column] == value){
                return true;
            }
        }
        return false;
    }

    boolean hasSameValueInRegion(Position position, int value){
        int regionTopLeftCornerRow = (position.getRow()/3)*3;
        int regionTopLeftCornerColumn = (position.getColumn()/3)*3;

        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(grid[regionTopLeftCornerRow+i][regionTopLeftCornerColumn+j] == value){
                    return true;
                }
            }
        }
        return false;
    }
}
