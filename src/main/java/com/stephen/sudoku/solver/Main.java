package com.stephen.sudoku.solver;

import com.stephen.sudoku.solver.utils.IOHelper;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner inputReader = new Scanner(System.in);

        System.out.println("Please enter filename (or full file path if not in current directory) of the CSV file containing the unsolved sudoku:");
        String unsolvedSudokuPathname = inputReader.nextLine();

        try{
            int[][] unsolvedSudoku = IOHelper.getUnsolvedSudokuFromFile(unsolvedSudokuPathname);
            SudokuSolver sudokuSolver = new SudokuSolver(unsolvedSudoku);

            if(sudokuSolver.hasSolution()){
                IOHelper.printSolvedSudokuToFile(sudokuSolver.getSolution(), "solved_sudoku.csv");
            } else {
                System.out.println("There is no solution for the input sudoku.");
            }
        } catch (IllegalArgumentException | IOException e){
            System.out.println("Error encountered: " + e.getMessage());
            System.out.println("Program will now exit.");
            System.exit(0);
        }
    }
}
