package com.stephen.sudoku.solver.constructs;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PositionTest {
    private Position position;

    @Before
    public void setup(){
        position = new Position(0, 8);
    }

    @Test
    public void testGetRowNormal(){
        assertEquals(0, position.getRow());
    }

    @Test(expected = AssertionError.class)
    public void testGetRowFail(){
        assertEquals(1, position.getRow());
    }

    @Test
    public void testGetColumnNormal(){
        assertEquals(8, position.getColumn());
    }

    @Test(expected = AssertionError.class)
    public void testGetColumnFail(){
        assertEquals(9, position.getColumn());
    }

}
