package com.stephen.sudoku.solver.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class IOHelperPassUnsolvedSudokuTest {
    List<String> unsolvedSudoku;

    @Before
    public void setup(){
        unsolvedSudoku = new ArrayList<String>(){{
            add("0,3,5,2,9,0,8,6,4");
            add("0,8,2,4,1,0,7,0,3");
            add("7,6,4,3,8,0,0,9,0");
            add("2,1,8,7,3,9,0,4,0");
            add("0,0,0,8,0,4,2,3,0");
            add("0,4,3,0,5,2,9,7,0");
            add("4,0,6,5,7,1,0,0,9");
            add("3,5,9,0,2,8,4,1,7");
            add("8,0,0,9,0,0,5,2,6");
        }};
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseUnsolvedSudokuWithExtraRow(){
        unsolvedSudoku.add("8,0,0,9,0,0,5,2,6");
        IOHelper.parseUnsolvedSudoku(unsolvedSudoku);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseUnsolvedSudokuWithExtraColumn(){
        unsolvedSudoku.set(8, "8,0,0,9,0,0,5,2,6,1,1,1");
        IOHelper.parseUnsolvedSudoku(unsolvedSudoku);
    }

    @Test
    public void testParseUnsolvedSudoku(){
        int[][] parsedUnsolvedSudoku = IOHelper.parseUnsolvedSudoku(unsolvedSudoku);

        int[][] expectedParsedUnsolvedSudoku = new int[][]{
                {0,3,5,2,9,0,8,6,4},
                {0,8,2,4,1,0,7,0,3},
                {7,6,4,3,8,0,0,9,0},
                {2,1,8,7,3,9,0,4,0},
                {0,0,0,8,0,4,2,3,0},
                {0,4,3,0,5,2,9,7,0},
                {4,0,6,5,7,1,0,0,9},
                {3,5,9,0,2,8,4,1,7},
                {8,0,0,9,0,0,5,2,6}
        };

        assertArrayEquals(expectedParsedUnsolvedSudoku, parsedUnsolvedSudoku);
    }
}
