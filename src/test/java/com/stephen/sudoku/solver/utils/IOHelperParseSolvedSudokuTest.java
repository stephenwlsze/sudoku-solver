package com.stephen.sudoku.solver.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class IOHelperParseSolvedSudokuTest {
    int[][] solvedSudoku;

    @Before
    public void setup(){
        solvedSudoku = new int[][]{
                {1,3,5,2,9,7,8,6,4},
                {9,8,2,4,1,6,7,5,3},
                {7,6,4,3,8,5,1,9,2},
                {2,1,8,7,3,9,6,4,5},
                {5,9,7,8,6,4,2,3,1},
                {6,4,3,1,5,2,9,7,8},
                {4,2,6,5,7,1,3,8,9},
                {3,5,9,6,2,8,4,1,7},
                {8,7,1,9,4,3,5,2,6}
        };
    }

    @Test
    public void testParseSolvedSudokuWithExtraRow(){
        solvedSudoku = new int[][]{
                {1,3,5,2,9,7,8,6,4},
                {9,8,2,4,1,6,7,5,3},
                {7,6,4,3,8,5,1,9,2},
                {2,1,8,7,3,9,6,4,5},
                {5,9,7,8,6,4,2,3,1},
                {6,4,3,1,5,2,9,7,8},
                {4,2,6,5,7,1,3,8,9},
                {3,5,9,6,2,8,4,1,7},
                {8,7,1,9,4,3,5,2,6},
                {8,7,1,9,4,3,5,2,6}
        };
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals(9, parsedSolvedSudoku.size());
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testParseSolvedSudokuWithLessRow(){
        solvedSudoku = new int[][]{
                {1,3,5,2,9,7,8,6,4},
                {9,8,2,4,1,6,7,5,3},
                {7,6,4,3,8,5,1,9,2},
                {2,1,8,7,3,9,6,4,5},
                {5,9,7,8,6,4,2,3,1},
                {6,4,3,1,5,2,9,7,8}
        };
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
    }

    @Test(expected = AssertionError.class)
    public void testParseSolvedSudokuRow0Fail(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertNotEquals("1,3,5,2,9,7,8,6,4\n", parsedSolvedSudoku.get(0));
    }

    @Test
    public void testParseSolvedSudokuRow0(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals("1,3,5,2,9,7,8,6,4\n", parsedSolvedSudoku.get(0));
    }

    @Test
    public void testParseSolvedSudokuRow1(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals("9,8,2,4,1,6,7,5,3\n", parsedSolvedSudoku.get(1));
    }

    @Test
    public void testParseSolvedSudokuRow2(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals("7,6,4,3,8,5,1,9,2\n", parsedSolvedSudoku.get(2));
    }

    @Test
    public void testParseSolvedSudokuRow3(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals("2,1,8,7,3,9,6,4,5\n", parsedSolvedSudoku.get(3));
    }

    @Test
    public void testParseSolvedSudokuRow4(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals("5,9,7,8,6,4,2,3,1\n", parsedSolvedSudoku.get(4));
    }

    @Test
    public void testParseSolvedSudokuRow5(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals("6,4,3,1,5,2,9,7,8\n", parsedSolvedSudoku.get(5));
    }

    @Test
    public void testParseSolvedSudokuRow6(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals("4,2,6,5,7,1,3,8,9\n", parsedSolvedSudoku.get(6));
    }

    @Test
    public void testParseSolvedSudokuRow7(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals("3,5,9,6,2,8,4,1,7\n", parsedSolvedSudoku.get(7));
    }

    @Test
    public void testParseSolvedSudokuRow8(){
        List<String> parsedSolvedSudoku = IOHelper.parseSolvedSudoku(solvedSudoku);
        assertEquals("8,7,1,9,4,3,5,2,6", parsedSolvedSudoku.get(8));
    }
}
