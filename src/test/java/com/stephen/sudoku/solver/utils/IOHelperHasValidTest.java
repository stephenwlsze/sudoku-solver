package com.stephen.sudoku.solver.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class IOHelperHasValidTest {
    List<String> unsolvedSudoku;

    @Before
    public void setup(){
        unsolvedSudoku = new ArrayList<String>(){{
            add("0,3,5,2,9,0,8,6,4");
            add("0,8,2,4,1,0,7,0,3");
            add("7,6,4,3,8,0,0,9,0");
            add("2,1,8,7,3,9,0,4,0");
            add("0,0,0,8,0,4,2,3,0");
            add("0,4,3,0,5,2,9,7,0");
            add("4,0,6,5,7,1,0,0,9");
            add("3,5,9,0,2,8,4,1,7");
            add("8,0,0,9,0,0,5,2,6");
        }};
    }

    @Test(expected = AssertionError.class)
    public void testHasValidNumberOfColumnFail(){
        assertFalse(IOHelper.hasValidNumberOfColumn(unsolvedSudoku));
    }

    @Test
    public void testHasValidNumberOfColumnWithExtraColumn(){
        unsolvedSudoku.set(8, "8,0,0,9,0,0,5,2,6,8");
        assertFalse(IOHelper.hasValidNumberOfColumn(unsolvedSudoku));
    }

    @Test
    public void testHasValidNumberOfColumnNormal(){
        assertTrue(IOHelper.hasValidNumberOfColumn(unsolvedSudoku));
    }

    @Test
    public void testHasValidNumberOfRowWithExtraRow(){
        unsolvedSudoku.add("8,0,0,9,0,0,5,2,6");
        assertFalse(IOHelper.hasValidNumberOfRow(unsolvedSudoku));
    }

    @Test
    public void testHasValidNumberOfRowNormal(){
        assertTrue(IOHelper.hasValidNumberOfRow(unsolvedSudoku));
    }

    @Test
    public void testHasValidDimensionWithExtraColumn(){
        unsolvedSudoku.set(8, "8,0,0,9,0,0,5,2,6,8");
        assertFalse(IOHelper.hasValidDimension(unsolvedSudoku));
    }

    @Test
    public void testHasValidDimensionWithExtraRow(){
        unsolvedSudoku.add("8,0,0,9,0,0,5,2,6");
        assertFalse(IOHelper.hasValidDimension(unsolvedSudoku));
    }

    @Test
    public void testHasValidDimensionNormal(){
        assertTrue(IOHelper.hasValidDimension(unsolvedSudoku));
    }
}
