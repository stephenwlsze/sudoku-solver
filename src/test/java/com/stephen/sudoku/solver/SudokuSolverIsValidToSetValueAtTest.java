package com.stephen.sudoku.solver;

import static org.junit.Assert.*;

import com.stephen.sudoku.solver.constructs.Position;
import org.junit.Before;
import org.junit.Test;

public class SudokuSolverIsValidToSetValueAtTest {
    private SudokuSolver sudokuSolver;

    @Before
    public void setup(){
        int[][] unsolvedSudoku = new int[][]{
                {0,3,5,2,9,0,8,6,4},
                {0,8,2,4,1,0,7,0,3},
                {7,6,4,3,8,0,0,9,0},
                {2,1,8,7,3,9,0,4,0},
                {0,0,0,8,0,4,2,3,0},
                {0,4,3,0,5,2,9,7,0},
                {4,0,6,5,7,1,0,0,9},
                {3,5,9,0,2,8,4,1,7},
                {8,0,0,9,0,0,5,2,6}
        };

        sudokuSolver = new SudokuSolver(unsolvedSudoku);
    }

    @Test(expected = AssertionError.class)
    public void testIsValidToSetValueAtWithDuplicateRowValueFail(){
        Position position = new Position(1, 0);
        assertTrue(sudokuSolver.isValidToSetValueAt(position, 1));
    }

    @Test
    public void testIsValidToSetValueAtWithDuplicateRowValue(){
        Position position = new Position(1, 0);
        assertFalse(sudokuSolver.isValidToSetValueAt(position, 1));
    }

    @Test
    public void testIsValidToSetValueAtWithDuplicateColumnValue(){
        Position position = new Position(8, 2);
        assertFalse(sudokuSolver.isValidToSetValueAt(position, 3));
    }

    @Test
    public void testIsValidToSetValueAtWithDuplicateRegionValue(){
        Position position = new Position(1, 6);
        assertFalse(sudokuSolver.isValidToSetValueAt(position, 6));
    }

    @Test
    public void testIsValidToSetValueAtNormal(){
        Position position = new Position(0, 5);
        assertTrue(sudokuSolver.isValidToSetValueAt(position, 7));
    }
}
