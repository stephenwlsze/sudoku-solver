package com.stephen.sudoku.solver;

import static org.junit.Assert.*;

import com.stephen.sudoku.solver.constructs.Position;
import org.junit.Before;
import org.junit.Test;

public class SudokuSolverSolveTest {
    private SudokuSolver sudokuSolver;

    @Before
    public void setup(){
        int[][] unsolvedSudoku = new int[][]{
                {0,3,5,2,9,0,8,6,4},
                {0,8,2,4,1,0,7,0,3},
                {7,6,4,3,8,0,0,9,0},
                {2,1,8,7,3,9,0,4,0},
                {0,0,0,8,0,4,2,3,0},
                {0,4,3,0,5,2,9,7,0},
                {4,0,6,5,7,1,0,0,9},
                {3,5,9,0,2,8,4,1,7},
                {8,0,0,9,0,0,5,2,6}
        };

        sudokuSolver = new SudokuSolver(unsolvedSudoku);
    }

    @Test
    public void testSolveNormal(){
        int[][] expectedSolvedSudoku = new int[][]{
                {1,3,5,2,9,7,8,6,4},
                {9,8,2,4,1,6,7,5,3},
                {7,6,4,3,8,5,1,9,2},
                {2,1,8,7,3,9,6,4,5},
                {5,9,7,8,6,4,2,3,1},
                {6,4,3,1,5,2,9,7,8},
                {4,2,6,5,7,1,3,8,9},
                {3,5,9,6,2,8,4,1,7},
                {8,7,1,9,4,3,5,2,6}
        };

        sudokuSolver.solve(new Position(0, 0));
        assertArrayEquals(expectedSolvedSudoku, sudokuSolver.getSolution());
    }
}
