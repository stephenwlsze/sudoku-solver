package com.stephen.sudoku.solver;

import static org.junit.Assert.*;

import com.stephen.sudoku.solver.constructs.Position;
import org.junit.Before;
import org.junit.Test;


public class SudokuSolverHasSameValueTest{
    private SudokuSolver sudokuSolver;

    @Before
    public void setup(){
        int[][] unsolvedSudoku = new int[][]{
                {0,3,5,2,9,0,8,6,4},
                {0,8,2,4,1,0,7,0,3},
                {7,6,4,3,8,0,0,9,0},
                {2,1,8,7,3,9,0,4,0},
                {0,0,0,8,0,4,2,3,0},
                {0,4,3,0,5,2,9,7,0},
                {4,0,6,5,7,1,0,0,9},
                {3,5,9,0,2,8,4,1,7},
                {8,0,0,9,0,0,5,2,6}
        };

        sudokuSolver = new SudokuSolver(unsolvedSudoku);
    }

    @Test(expected = AssertionError.class)
    public void testHasSameValueInRowWithDuplicateFail(){
        assertFalse(sudokuSolver.hasSameValueInRow(0, 5));
    }

    @Test
    public void testHasSameValueInRowWithDuplicate(){
        assertTrue(sudokuSolver.hasSameValueInRow(0, 5));
    }

    @Test
    public void testHasSameValueInRowNormal(){
        assertFalse(sudokuSolver.hasSameValueInRow(0, 1));
    }

    @Test
    public void testHasSameValueInColumnWithDuplicate(){
        assertTrue(sudokuSolver.hasSameValueInColumn(0, 7));
    }

    @Test
    public void testHasSameValueInColumnNormal(){
        assertFalse(sudokuSolver.hasSameValueInColumn(0, 1));
    }

    @Test
    public void testHasSameValueInRegionWithDuplicate(){
        Position position = new Position(0, 5);
        assertTrue(sudokuSolver.hasSameValueInRegion(position, 1));
    }

    @Test
    public void testHasSameValueInRegionNormal(){
        Position position = new Position(0, 5);
        assertFalse(sudokuSolver.hasSameValueInRegion(position, 6));
    }
}
